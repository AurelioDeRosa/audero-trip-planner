function AuderoMap(trip, id) {
   this.tripData = trip;
   this.mapsElementId = id;
   this.paths = null;
   this.selectedId = 0;
}

AuderoMap.prototype.getOrigin = function() {
   return this.pointToLatLng(this.tripData.origin);
}

AuderoMap.prototype.getDestination = function() {
   return this.pointToLatLng(this.tripData.destination);
}

AuderoMap.prototype.getRoute = function(routeId) {
   return this.tripData.path.routes[routeId];
}

AuderoMap.prototype.pointToLatLng = function(point) {
   return new google.maps.LatLng(point.lat, point.lng);
}


AuderoMap.prototype.initPath = function(step) {
   this.paths = new Array();
   for (var i = 0; i < step; i++)
      this.paths.push(undefined);
}

AuderoMap.prototype.isPathComplete = function() {
   for (var i = 0; i < this.paths.length; i++) {
      if (this.paths[i] === undefined)
         return false;
   }

   return true;
}
AuderoMap.prototype.calculateRoute = function(routeId) {
   this.selectedId = routeId;
   var route = this.getRoute(routeId);

   this.initPath(route.length);
   for (var step = 0; step < route.length; step++) {
      if (route[step].type == "bus")
         this.getRouteWayPoints(route[step], step);
      else {
         this.paths[step] = this.getLineWayPoints(route[step]);
         // incremento della progressbar
         $("#progressBar")
         .attr(
            "value",
            $("#progressBar").attr("value")
            + ($("#progressBar").attr("max") / this.paths.length));
         $("#progressBar").text = $("#progressBar").attr("value");
      }
   }

}

AuderoMap.prototype.displayMap = function(mapId) {
   var mergePath = $.map(this.paths, function(i) {
      return i;
   });
   mergePath.unshift(this.getOrigin());
   mergePath.push(this.getDestination());

   var myOptions = {
      zoom : 20,
      center : mergePath[0],
      mapTypeId : google.maps.MapTypeId.ROADMAP
   }
   var map = new google.maps.Map(document.getElementById(mapId), myOptions);

   var latlngbounds = new google.maps.LatLngBounds();
   for ( var i = 0; i < mergePath.length; i++)
      latlngbounds.extend(mergePath[i]);
   map.fitBounds(latlngbounds);

   var poly = new google.maps.Polyline({
      map: map
   });

   this.addMarkers(map);
   poly.setPath(mergePath);
   this.getEstimatedTime();
   this.getEstimatedCO2();

   var context = this;
   $.getJSON(
      "database/database-bike.json",
      function(data) {
         context.viewBikePark(data, map);
      }
      );

}

AuderoMap.prototype.addMarkers = function(map) {
   var tempRoute = this.getRoute(this.selectedId);
   var resultMarker = new Array();
   var typeTransport;
   var coordTemp;
   var i;

   coordTemp = this.tripData.origin;
   coordTemp.type = tempRoute[0].type;
   coordTemp.use = "origin";
   resultMarker.push(coordTemp);

   for (i = 0; i < tempRoute.length; i++) {
      typeTransport = tempRoute[i].type;
      var arrayCoord = tempRoute[i].coords;
      // al posto di fare il controllo se l'elemento è gia in lista,
      // salto l'utlimo elemento di ogni mezzo perché è la copia del
      // primo elemento del mezzo successivo
      for ( var j = 0; j < arrayCoord.length - 1; j++) {
         coordTemp = arrayCoord[j]
         coordTemp.type = typeTransport;
         if (j == 0 && i == 0 || j != 0)
            coordTemp.use = "intermediate";
         else if (j == 0)
            coordTemp.use = "change";
         resultMarker.push(coordTemp);
      }

   }
   // e inserisco la destinazione ultima
   coordTemp = this.tripData.destination;
   coordTemp.use = "destination"
   coordTemp.type = typeTransport;

   resultMarker.push(coordTemp);

   for (i = 0; i < resultMarker.length; i++) {
      var marker = new google.maps.Marker({
         position : this.pointToLatLng(resultMarker[i]),
         map : map,
         title : resultMarker[i].use
      });
      marker.setIcon(this.getImageByTypeTransport(resultMarker[i].type,
         resultMarker[i].use));
   }

}

AuderoMap.prototype.getImageByTypeTransport = function(typeTransport, use) {
   var image = "images/";
   if (use == "intermediate")
      image += "intermediate-";

   return image + typeTransport + ".png";
}

AuderoMap.prototype.getRouteWayPoints = function(step, index) {
   var wayPoints = new Array();
   var routeCoords;
   var directionsService = new google.maps.DirectionsService();

   routeCoords = step.coords;
   for ( var i = 0; i < routeCoords.length; i++) {
      wayPoints.push({
         "location" : this.pointToLatLng(routeCoords[i]),
         "stopover" : (i == 0 || i == routeCoords.length - 1)
      });
   }

   var request = {
      origin : wayPoints.shift().location,
      destination : wayPoints.pop().location,
      travelMode : google.maps.DirectionsTravelMode.DRIVING,
      waypoints : wayPoints,
      unitSystem : google.maps.UnitSystem.METRIC
   };

   var context = this;
   directionsService
   .route(
      request,
      function(response, status) {
         if (status == google.maps.DirectionsStatus.OK)
            context.paths[index] = response.routes[0].overview_path;
         if (context.isPathComplete())
            context.displayMap(context.mapsElementId);
         $("#progressBar")
         .attr(
            "value",
            $("#progressBar").attr("value")
            + ($("#progressBar").attr(
               "max") / context.paths.length));
         $("#progressBar").text = $("#progressBar").attr(
            "value");
      });
}

AuderoMap.prototype.getLineWayPoints = function(step) {
   var wayPoints = new Array();
   var routeCoords;

   routeCoords = step.coords;
   for ( var i = 0; i < routeCoords.length; i++)
      wayPoints.push(this.pointToLatLng(routeCoords[i]));

   return wayPoints;
}

AuderoMap.prototype.pointsEqual = function(point1, point2) {
   try {
      return (point1.location.lat() === point2.location.lat() && point1.location.lng() === point2.location.lng()) ?
         true :
         false;
   } catch (ex) {
      return false;
   }
}

AuderoMap.prototype.getEstimatedTime = function() {
   var startTime = new Date(this.tripData.path.routes[this.selectedId][0].startTime);
   var endTime = new Date(this.tripData.path.routes[this.selectedId][this.tripData.path.routes[this.selectedId].length - 1].endTime);
   var diff = endTime.getTime() - startTime.getTime();
   diff = Math.round(diff / (1000 * 60));
   $("#tempo-stimato-" + this.selectedId).text(diff + " min");
}

AuderoMap.prototype.getEstimatedCO2 = function() {
   var route = this.tripData.path.routes[this.selectedId];
   var tot = 0;
   for (var i = 0; i < route.length; i++) {
      tot += route[i].co2;
   }
   $("#co2-" + this.selectedId).text(tot + " Kg");

}

AuderoMap.prototype.viewBikePark = function(data, map) {
   var image = this.getImageByTypeTransport("cycling", "");
   var context = this;
   $.each(
      data.stations,
      function(index, entry) {
         var marker = new google.maps.Marker({
            position : context.pointToLatLng(entry.coords),
            map : map,
            title : data.stations[index].name
         });
         marker.setIcon(image);
         marker.info = new google.maps.InfoWindow(
         {
            content : "<p>3 bici disponibili</p><p>4 parcheggi disponibili</p>"
         });

         google.maps.event.addListener(marker, "click",
            function() {
               marker.info.open(map, marker);
            });
      }
   );
}