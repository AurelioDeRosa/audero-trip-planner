﻿# Audero Trip Planner #
[Audero Trip Planner](https://bitbucket.org/AurelioDeRosa/audero-trip-planner) (A.T.P.) è il progetto di un'applicazione con la quale noi, il [team Audero](http://www.audero.it), [abbiamo vinto l'hackathon della mobilità 2012](http://ug.audero.it/2012/06/16/mobilitytech-2012-audero-vince-l-hackathon-di-napoli/) tenutosi durante il [MobilityTech a Napoli](http://www.mobilitytech.it/italian/index.php). Per chi non ne abbia sentito parlare, A.T.P. è un servizio integrato che include tutte le categorie di trasporto di una provincia con particolare attenzione agli aspetti sociali. [La presentazione del progetto](http://prezi.com/7dsv3drbdoev/atp-audero-trip-planner/) può essere visionata cliccando [qui](http://prezi.com/7dsv3drbdoev/atp-audero-trip-planner/)

In particolare, [Audero Trip Planner](https://bitbucket.org/AurelioDeRosa/audero-trip-planner) una sorta di navigatore per il trasporto pubblico per l'intera provincia di Napoli (ma l'idea è territorialmente estendibile) che, integrando i dati di tutte le compagnie operanti nel settore, offra il percorso migliore possibile per arrivare ad una certa destinazione. L'idea è nata analizzando l'attuale situazione dei trasporti a Napoli dove operano moltissime compagnie. Ciò rende ostico l'uso del trasporto pubblico a persone poco esperte o ai turisti che non sanno bene come muoversi. I punti cardine dell'applicazione sono: innovazione, economicità, ecologia e sostenibilità, servici e social.

Il progetto è stato pensato per essere realizzato con l'uso di jQuery Mobile e Cordova (PhoneGap), ovvero tecnologie che permettono di scrivere il codice una sola volta per poi rilasciare il software come applicazione nativa per i più moderni sistemi mobili e, con pochissimi ritocchi, è anche possibile rilasciarla come servizio web. Il vantaggio è che in questo modo esso può essere utilizzato anche attraverso un normale computer oppure come alternativa per i sistemi mobili non supportati. Audero Trip Planner è un servizio economico poiché dovendo scrivere un solo codice sorgente, è possibile ridurre i costi di sviluppo, i tempi di rilascio e, contemporaneamente, fornire un servizio che copra tutti i sistemi operativi sul mercato, siano essi mobili o desktop.

L'applicazione è in realtà una demo di quello che è il progetto finale volto a far comprendere le sue potenzialità.

## Compilazione ##
["Audero Trip Planner"](https://bitbucket.org/AurelioDeRosa/audero-trip-planner) è stato sviluppato con il presupposto che la compilazione verrà effettuata usando il servizio [Adobe PhoneGap Build](http://build.phonegap.com/), ma con alcuni piccoli accorgimenti può essere compilata usando l'ambiente di sviluppo specifico per ogni piattaforma mobile.

## Compatibilità ##
Essendo un'applicazione basata su Cordova, può essere eseguita su tutte le piattaforme supportate dal framework. Per avere un elenco completo dei sistemi operativi supportati, potete leggere la [documentazione ufficiale](http://phonegap.com/about/feature).

## License ##
A.T.P. è rilasciato sotto licenza [CC BY 3.0](http://creativecommons.org/licenses/by/3.0/) ("Creative Commons Attribution 3.0").

## Autori ##
* [Aurelio De Rosa](http://www.audero.it) (Twitter: [@AurelioDeRosa](https://twitter.com/AurelioDeRosa)): [a.derosa@audero.it](mailto:a.derosa@audero.it)
* [Annarita Tranfici](http://www.audero.it): [a.tranfici@audero.it](mailto:a.tranfici@audero.it)
* [Mirko Cesaro](http://www.audero.it): [m.cesaro@audero.it](mailto:m.cesaro@audero.it)